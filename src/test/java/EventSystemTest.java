import com.enderzombi102.eventsystem.*;
import org.junit.jupiter.api.Test;
import org.slf4j.helpers.NOPLogger;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class EventSystemTest {
	private static final EventSystem SYSTEM;
	public static boolean hitEmpty = false;
	public static boolean hitFull;

	static {
		SYSTEM = new EventSystem( NOPLogger.NOP_LOGGER );
		SYSTEM.register( new Listener() );
	}

	@Test
	public void testParameterlessDispatch() {
		SYSTEM.dispatch( "dispatch.empty" );
		assertTrue( hitEmpty );
	}

	@Test
	public void testParameterDispatch() {
		SYSTEM.dispatch( "dispatch.full", 123 );
		assertTrue( hitFull );
	}

	@Test
	public void testEventCancelParameterless() {
		assertTrue( SYSTEM.dispatchCancellable( "cancellable.empty" ).isCancelled() );
	}

	@Test
	public void testEventCancelWithParams() {
		assertTrue( SYSTEM.dispatchCancellable( "cancellable.full", 123 ).isCancelled() );
	}


	public static void main( String[] argv ) {
		EventSystemTest test = new EventSystemTest();
		test.testParameterlessDispatch();
		test.testParameterDispatch();
		test.testEventCancelParameterless();
		test.testParameterDispatch();
	}
}

class Listener {
	@Listen( "dispatch.empty" )
	public void onEventWithoutParams() {
		EventSystemTest.hitEmpty = true;
	}

	@Listen( "dispatch.full" )
	public void onEventWithParams( int data ) {
		EventSystemTest.hitFull = data == 123;
	}

	@Listen( "cancellable.empty" )
	public void onCancellableEvent( CancellableFlag flag ) {
		flag.cancel();
	}

	@Listen( "cancellable.full" )
	public void onCancellableEvent( CancellableFlag flag, int data ) {
		if ( data == 123 )
			flag.cancel();
	}
}
