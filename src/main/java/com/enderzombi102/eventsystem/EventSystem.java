package com.enderzombi102.eventsystem;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;
import java.util.*;

import static com.enderzombi102.enderlib.collections.ArrayUtil.classes;
import static com.enderzombi102.enderlib.collections.ListUtil.append;
import static com.enderzombi102.enderlib.collections.ListUtil.mutableListOf;
import static com.enderzombi102.enderlib.reflection.Annotations.annotation;
import static com.enderzombi102.enderlib.reflection.Reflection.getImplLookup;
import static java.lang.reflect.Modifier.isPublic;
import static java.lang.reflect.Modifier.isStatic;
import static java.util.Collections.emptyList;

/**
 * The heart of the library.
 */
public class EventSystem {
	private final @NotNull Map< String, List<MethodHandle> > listeners = new HashMap<>();
	private final @NotNull Logger logger;

	/**
	 * Constructs the {@link EventSystem} with a default logger.
	 */
	public EventSystem() {
		this( LoggerFactory.getLogger( "EventSystem" ) );
	}

	/**
	 * Constructs the {@link EventSystem} with the provided logger.
	 * @param logger logger the system will use.
	 */
	public EventSystem( @NotNull Logger logger ) {
		this.logger = logger;
	}

	/**
	 * Dispatch cancellable events to the system.
	 * @param id id of the dispatched event.
	 * @param params event data to dispatch.
	 * @return the used CancellableFlag object.
	 */
	public CancellableFlag dispatchCancellable( @NotNull String id, Object... params ) {
		List<MethodHandle> funcs = listeners.getOrDefault( id, emptyList() );
		logger.debug( "Dispatching event {} to {} listeners.", id, funcs.size() );

		CancellableFlag flag = new CancellableFlag();
		Object[] eventParams = append( mutableListOf( flag ), params ).toArray( new Object[0] );
		for ( MethodHandle listener : funcs ) {
			checkAndCall( id, listener, eventParams );
			if ( flag.isCancelled() )
				break;
		}
		return flag;
	}

	/**
	 * Dispatch events to the system.
	 * @param id id of the dispatched event.
	 * @param params event data to dispatch.
	 */
	public void dispatch( @NotNull String id, Object... params ) {
		List<MethodHandle> funcs = listeners.getOrDefault( id, emptyList() );
		logger.debug( "Dispatching event {} to {} listeners.", id, funcs.size() );

		for ( MethodHandle listener : funcs )
			checkAndCall( id, listener, params );
	}

	/**
	 * Register all non-static methods annotated by {@link Listen} as event listeners.
	 * @param listener an object with methods annotated by {@link Listen}.
	 */
	public void register( @NotNull Object listener ) {
		logger.debug( "Searching class '{}' for event listeners", listener.getClass().getName() );
		String event;
		for ( Method method : listener.getClass().getDeclaredMethods() ) {
			logger.debug( "Checking method '{}'", method.getName() );
			if (
				isPublic( method.getModifiers() ) &&
				!isStatic( method.getModifiers() ) &&
				( event = annotation( method, Listen.class, Listen::value ) ) != null
			) {
				logger.debug( "Listener found, listens for event '{}'", event );
				try {
					listeners
						.computeIfAbsent( event, key -> new ArrayList<>() )
						.add( getImplLookup().unreflect( method ).bindTo( listener ) );
				} catch ( IllegalAccessException e ) {
					logger.error( "Exception while trying to add method " + method.getName() + " as listener for event " + event, e );
				}
			}
		}
	}

	private void checkAndCall( @NotNull String id, @NotNull MethodHandle listener, Object[] params ) {
		try {
			Class<?>[] clazzes = classes( params );
			if ( Arrays.equals( listener.type().parameterArray(), clazzes ) )
				listener.invokeWithArguments( params );
			else
				logger.error(
					"Failed to call listener '{}': Signature mismatch! expected {} but got {}",
					listener,
					clazzes,
					listener.type().parameterArray()
				);
		} catch ( Throwable e ) {
			logger.error( "Error while dispatching event '{}' to listener '{}'", id, listener );
		}
	}
}
