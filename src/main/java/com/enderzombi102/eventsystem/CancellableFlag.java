package com.enderzombi102.eventsystem;

/**
 * An object that is passed to listeners of cancellable events that represents an event's cancelled state.<br>
 * When an event is cancelled, it stops propagating to other listeners.
 */
public class CancellableFlag {
	private boolean cancelled = false;

	/**
	 * Whether the event was cancelled.
	 * @return true if it was, false otherwise.
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Cancels the event and stops propagation.
	 */
	public void cancel() {
		this.cancelled = true;
	}
}
