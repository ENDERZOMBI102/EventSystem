plugins {
    `maven-publish`
    java
}

repositories {
    mavenLocal()
    mavenCentral()
}

val log4j_version = "2.18.0"
val slf4j_version = "2.0.3"
val junit_version = "5.9.0"
val enderlib_version = "0.3.2-SNAPSHOT"
val annotations_version = "23.0.0"

dependencies {
    implementation( "org.slf4j:slf4j-api:$slf4j_version" )
    implementation( "com.enderzombi102.EnderLib:jvm8:$enderlib_version" )
    implementation( "org.jetbrains:annotations:$annotations_version" )

    testRuntimeOnly( "org.apache.logging.log4j:log4j-slf4j18-impl:$log4j_version" )
    testRuntimeOnly( "org.junit.jupiter:junit-jupiter-engine:$junit_version" )
    testImplementation( "org.junit.jupiter:junit-jupiter-api:$junit_version" )
}

tasks.withType<JavaCompile> {
    targetCompatibility = "8"
    sourceCompatibility = "8"
    options.encoding = "UTF-8"
}

tasks.withType<ProcessResources> {
    inputs.property( "version", version )

    filesMatching("MANIFEST.mf") {
        expand( ( "version" to version ) )
    }
}

java {
    withJavadocJar()
    withSourcesJar()
}

tasks.withType<Test> {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            group = "com.enderzombi102"
            artifactId = "eventsystem"
            from( components["java"] )
        }
    }

    repositories {
        mavenLocal()

        maven {
            name = "Repsy"
            credentials(PasswordCredentials::class)
            url = uri("https://repsy.io/mvn/enderzombi102/mc")
        }
    }
}