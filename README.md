EventSystem
-
The dumbest event system for the simplest purposes.


Using the library
-
#### Install using Gradle
```kotlin
repositories {
    maven( url="https://repsy.io/mvn/enderzombi102/mc" )
}

val eventsystem_version = "1.0.0"

dependencies {
    implementation( "com.enderzombi102:eventsystem:$eventsystem_version" )
}
```

#### Registering listeners
```jshelllanguage
import com.enderzombi102.eventsystem.EventSystem;

class Listener {
	@Listen("event.id")
	void method() {
		System.out.println("`event.id` was dispatched!");
	}
}

var system = new EventSystem(); // it is possible to specify what logger the system will use by passing an instance of org.slf4j.Logger
system.register( new Listener() ); // registers all listeners on this object
```

#### Dispatching events
```jshelllanguage
import com.enderzombi102.eventsystem.EventSystem;

var system = new EventSystem();
system.dispatch( "event.id", "some_event_data" ); // dispatches a _non-cancellable_ event
system.dispatchCancellable( "event.other.id", 123 ); // dispatches a _cancellable_ event
```